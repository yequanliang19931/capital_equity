# -*- coding: utf-8 -*-
"""
Created: Friday, August 04 2023
Description: script to draw a geo-info map using basemap lib
Scope: for the capital equity project
Author: Quanliang Ye
Institution: Aalborg University, Aalborg, Denmark; Nijmegen School of Management, Radboud University, Nijmegen, Netherlands
Email: yequanliang1993@gmail.com
"""

from pathlib import Path

import fiona
import geopandas as gpd
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.patches import Polygon
from mpl_toolkits.basemap import Basemap

home_path = Path.home()
project_root_path = (
    home_path
    / "OneDrive - Aalborg Universitet/Research ALL PROJECTS/(0 Processing) Equity across generation as commentary"
)

# ----------------------------------------
# CREATE A BASEMAP
# ----------------------------------------
map = Basemap(
    projection="cyl",
    lat_0=10,
    lon_0=13,
    llcrnrlon=-29,
    llcrnrlat=-36,
    urcrnrlon=63,
    urcrnrlat=38,
    resolution="c",
)

# ----------------------------------------
# READ SHAPE FILE OF COUNTRIES IN AFRICA
# ----------------------------------------
africa_shp_path = project_root_path / "Data/Africa_Boundaries/Africa_Boundaries"
africa_shp_name = "Africa_Boundaries"
map.readshapefile(
    africa_shp_path,
    africa_shp_name,
    drawbounds=True,
)

# ----------------------------------------
# DATA IMPORT
# ----------------------------------------
share_cap_path = project_root_path / "share_capacity.csv"
share_cap = pd.read_csv(share_cap_path)
share_cap = share_cap.set_index("country")

# ----------------------------------------
# GRO-INFO
# ----------------------------------------
fig = plt.gca()


# using patch function
def pcolor(seg, country_cap_share):
    colors = ""
    if country_cap_share < 0.2:
        colors = "#fbcdf9"  # hardcoding color codes
        label_ = "0 - 20%"  # labels for legend
    elif country_cap_share < 0.4:
        colors = "#f19e6a"
        label_ = "20 - 40%"
    elif country_cap_share < 0.6:
        colors = "#818332"
        label_ = "40 - 60%"
    elif country_cap_share < 0.8:
        colors = "#206061"
        label_ = "60 - 80%"
    elif country_cap_share <= 1.0:
        colors = "#02195a"
        label_ = "80 - 100%"
    else:
        colors = "w"
        label_ = "No Data"

    poly = Polygon(seg, facecolor=colors, alpha=0.4, label=label_)
    fig.add_patch(poly)


for seg, country_info in zip(map.Africa_Boundaries, map.Africa_Boundaries_info):
    country_name = country_info["NAME_0"]
    try:
        country_cap_share = share_cap.loc[country_name, "share"]
    except:
        country_cap_share = 9999
    pcolor(seg, country_cap_share)


# ----------------------------------------
# MARK LOCATIONS OF PROJECT ITEMS
# ----------------------------------------
# IMPORT KML DRIVER
fiona.drvsupport.supported_drivers["KML"] = "rw"

# Read KML file to a geopandas dataframe
kml_file_path = project_root_path / "FDI_CHN_items.kml"
geo_df = gpd.read_file(
    kml_file_path,
    driver="KML",
)

items = [
    "hydro",
    "solar",
    "wind",
    "nuclear plant",
    "power plant",
    "substation",
    "others",
]
item_marker_color = {}
for item in items:
    item_marker_color[item] = {}
    if item == "hydro":
        item_marker_color[item]["marker"] = "v"  # hardcoding markers
        item_marker_color[item]["color"] = "#428cbe"  # hardcoding color codes
    elif item == "solar":
        item_marker_color["solar"]["marker"] = "D"
        item_marker_color["solar"]["color"] = "#3f6e55"
    elif item == "wind":
        item_marker_color["wind"]["marker"] = "1"
        item_marker_color["wind"]["color"] = "#dacf6c"
    elif item == "nuclear plant":
        item_marker_color["nuclear plant"]["marker"] = "p"
        item_marker_color["nuclear plant"]["color"] = "#fcbed2"
    elif item == "power plant":
        item_marker_color["power plant"]["marker"] = "8"
        item_marker_color["power plant"]["color"] = "#fba686"
    elif item == "substation":
        item_marker_color["substation"]["marker"] = "."
        item_marker_color["substation"]["color"] = "#ac8c2c"
    elif item == "others":
        item_marker_color["others"]["marker"] = "o"
        item_marker_color["others"]["color"] = "#165261"

# ----------------------------------------
# EXTRACT POINTS, LINES, AND POLYGONS
# ----------------------------------------
geo_points = pd.DataFrame()
geo_lines = pd.DataFrame()
geo_polygons = pd.DataFrame()
for i in range(len(geo_df)):
    if "POINT" in str(geo_df.loc[i]["geometry"]):
        geo_points = pd.concat([geo_points, geo_df[i : i + 1]])

        if "hydro" in geo_df.loc[i, "Name"]:
            geo_points.loc[i, "item"] = "hydro"
        elif "solar" in geo_df.loc[i, "Name"]:
            geo_points.loc[i, "item"] = "solar"
        elif "wind" in geo_df.loc[i, "Name"]:
            geo_points.loc[i, "item"] = "wind"
        elif "nuclear" in geo_df.loc[i, "Name"]:
            geo_points.loc[i, "item"] = "nuclear plant"
        elif "power plant" in geo_df.loc[i, "Name"]:
            geo_points.loc[i, "item"] = "power plant"
        elif "substation" in geo_df.loc[i, "Name"]:
            geo_points.loc[i, "item"] = "substation"
        elif "others" in geo_df.loc[i, "Name"]:
            geo_points.loc[i, "item"] = "others"

    elif "LINESTRING" in str(geo_df.loc[i]["geometry"]):
        geo_lines = pd.concat([geo_lines, geo_df[i : i + 1]])

    elif "POLYGON" in str(geo_df.loc[i]["geometry"]):
        geo_polygons = pd.concat([geo_polygons, geo_df[i : i + 1]])
        if "solar" in geo_df.loc[i, "Name"]:
            geo_polygons.loc[i, "item"] = "solar"
        elif "wind" in geo_df.loc[i, "Name"]:
            geo_polygons.loc[i, "item"] = "wind"

# ----------------------------------------
## PLOT POINTS
# ----------------------------------------
geo_points["longitude"] = geo_points.geometry.apply(lambda p: p.x)
geo_points["latitude"] = geo_points.geometry.apply(lambda p: p.y)
geo_points["z"] = geo_points.geometry.apply(lambda p: p.z)
geo_points = geo_points.reset_index(drop=True)
# geo_points.to_csv("info_all.csv")

for i in range(len(geo_points)):
    point = map(
        geo_points.loc[i, "longitude"],
        geo_points.loc[i, "latitude"],
    )
    item = geo_points.loc[i, "item"]
    if item == "wind":
        map.scatter(
            point[0],
            point[1],
            marker=item_marker_color[item]["marker"],
            color=item_marker_color[item]["color"],
            edgecolors=item_marker_color[item]["color"],
            s=100,
            label=item,
        )
    else:
        map.scatter(
            point[0],
            point[1],
            marker=item_marker_color[item]["marker"],
            color=item_marker_color[item]["color"],
            edgecolors=item_marker_color[item]["color"],
            label=item,
        )

# ----------------------------------------
## POINT LINES
# ----------------------------------------
geo_lines = geo_lines.reset_index(drop=True)
for i in range(len(geo_lines)):
    line = str(geo_lines.loc[i, "geometry"]).split("(")[-1].split(")")[0].split(", ")
    line_points_longi = [float(point_.split()[0]) for point_ in line]
    line_points_lati = [float(point_.split()[1]) for point_ in line]
    line_points = map(line_points_longi, line_points_lati)
    map.plot(
        line_points[0],
        line_points[1],
        color="#7e1900",
        label="line",
    )

# ----------------------------------------
# POLYGON PLOT
# ----------------------------------------
geo_polygons = geo_polygons.reset_index(drop=True)
for i in range(len(geo_polygons)):
    item = geo_polygons.loc[i, "item"]
    polygon = (
        str(geo_polygons.loc[i, "geometry"]).split("((")[-1].split("))")[0].split(", ")
    )
    polygon_points_longi = [float(polygon_.split()[0]) for polygon_ in polygon]
    polygon_points_lati = [float(polygon_.split()[1]) for polygon_ in polygon]
    polygon_points = map(polygon_points_longi, polygon_points_lati)
    map.plot(
        polygon_points[0],
        polygon_points[1],
        color=item_marker_color[item]["color"],
        label="field",
    )

# ----------------------------------------
# SET LEGEND
# ----------------------------------------
handles, labels = fig.get_legend_handles_labels()
fontsize = 7

# legend for capacity share
leg_handles = []
leg_labels = []
for share_range in [
    "0 - 20%",
    "20 - 40%",
    "40 - 60%",
    "60 - 80%",
    "80 - 100%",
    "No Data",
]:
    index = labels.index(share_range)
    leg_handles.append(handles[index])
    leg_labels.append(labels[index])
legend1 = plt.legend(
    leg_handles,
    leg_labels,
    edgecolor=None,
    loc=(0.25, 0.090),
    title="Share in capacity",
    title_fontsize=fontsize,
    # alignment="left",
    fontsize=fontsize,
)
frame = legend1.get_frame()
frame.set_facecolor("none")
frame.set_edgecolor("none")
fig.add_artist(legend1)

# legend for project
leg_handles = []
leg_labels = []
for item in items + ["line"]:
    index = labels.index(item)
    item_number = labels.count(item)
    leg_handles.append(handles[index])
    leg_labels.append(labels[index] + " (" + str(item_number) + ")")
legend2 = plt.legend(
    leg_handles,
    leg_labels,
    edgecolor=None,
    loc="lower left",
    title="Project (total number)",
    title_fontsize=fontsize,
    # alignment="left",
    fontsize=fontsize,
)
frame = legend2.get_frame()
frame.set_facecolor("none")
frame.set_edgecolor("none")

# ----------------------------------------
# SAVE FIGURE
# ----------------------------------------
export_file_name = "figure_fdi_geoinfo.png"
plt.savefig(
    project_root_path / export_file_name,
    dpi=600,
    facecolor="none",
    alpha=0,
)

plt.show()
